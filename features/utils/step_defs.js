var common = require("../lib/pageObjects.js")
var cacheBust = '?' + (new Date).getTime();

module.exports = function() {

  this.Given(/^I am on the "([^"]*)" page$/, function (location) {
    // browser.url(common.siteSettings.home);
    // browser.pause(500);
    browser.url(common.siteSettings[location] + '?popup=off');
    client.execute(function(){
       localStorage.setItem('popupShownInFuture', 1750041249345)
     });
    // browser.pause(5000);
    // var output = client.execute(function(){
    //   return localStorage.getItem('popupShownInFuture');
    //  })
    //  console.log(output);
  });

  this.Then(/^I clear the cache$/, function () {
    // browser.url(common.siteSettings.home);
    browser.execute(function(){
      window.localStorage.clear();
    });
    browser.pause(500);
    //  client.execute(function(){
    //    localStorage.setItem('popupShownInFuture', 1721708130141)
    //  });
    //  var output = client.execute(function(){
    //    return localStorage.getItem('popupShownInFuture');
    //  })
    //  console.log(output);
    // browser.localStorage('POST', {key: 'popupShownInFuture', value: '1721708130141'});
    // browser.pause(50000);
  });

    this.Then(/^I inject the popup cookie$/, function () {
     browser.url(common.siteSettings.home);
     browser.pause(500);
     client.execute(function(){
       localStorage.setItem('popupShownInFuture', 1721708130141)
     });
    //  var output = client.execute(function(){
    //    return localStorage.getItem('popupShownInFuture');
    //  })
    //  console.log(output);
    // browser.localStorage('POST', {key: 'popupShownInFuture', value: '1721708130141'});
  });

  this.Then(/^I close the popup$/, function () {
    // browser.waitForExist(common.pageObjects.closeModal, 5000);
    browser.click(common.pageObjects.closeModal);
  });

  this.Then(/^I setup the tests$/, function () {
    browser.addCommand("uiAutocomplete", function (selector){
      browser.pause(5000);
      client.execute(function(){
        jQuery('.ui-autocomplete').css('display', 'block');
      })
      client.waitForVisible('//a[contains(@class, "ui-corner-all") and contains(., "' + selector + '")]', 30000);
      client.click('//a[contains(@class, "ui-corner-all") and contains(., "' + selector + '")]');
      client.execute(function(){
        jQuery('.ui-autocomplete').css('display', 'none');
      })
    });
  });

  this.Then(/^I fill in the "([^"]*)" field with "([^"]*)"$/, function (selector, value) {
    browser.waitForExist(common.pageObjects[selector], 10000);
    client.setValue(common.pageObjects[selector], value);
  });

  this.Then(/^I click the "([^"]*)"$/, function (selector) {
    client.click(common.pageObjects[selector]);
  });

  this.Then(/^I set the "([^"]*)" to display$/, function (selector) {
    client.execute(jQuery(common.pageObjects[selector]).css('display', 'block'));
  });

  this.Then(/^I should see "([^"]*)" in the title$/, function (text) {
    client.waitForExist('h1', 5000);
    var header = client.getText('h1');
    expect(header).toContain(text);
  });

  this.Then(/^I should see "([^"]*)" in the url$/, function (partial) {
    browser.waitUntil(function(){
      if (browser.url().value.indexOf(partial) > -1){
        return true;
      }
    }, 45000)
    var found = browser.url().value.indexOf(partial) > -1;
    expect(found).toBe(true);
  });

  this.Then(/^I should see "([^"]*)" on "([^"]*)"$/, function (text, selector) {
    client.waitForExist(common.pageObjects[selector], 30000);
    expect(client.getText(common.pageObjects[selector])).toEqual(text);
  });

  this.Given(/^I wait for a period of time$/, function () {
    browser.pause(10000);
  });

  this.When(/^I switch tabs$/, function () {
    browser.switchTab(2);
  });

  this.Then(/^I should see that there are "([^"]*)" "([^"]*)"$/, function (count, selector) {
    browser.waitUntil(function(){
      return browser.elements(common.pageObjects[selector], function(err, res) {
        if (res.value.length == count){
          return true;
        };
      });
    }, 30000);
  });
}
