var common = require("../../lib/pageObjects.js");

module.exports = function() {

  this.Given(/^that i am logged in as an administrator$/, function () {
    browser.url(common.siteSettings.login);
    client.click(common.pageObjects.githubLoginButton);
  });

//   this.Then(/^I click the first bookable product$/, function () {
//     console.log('hello first bookable');
//     console.log(client.element(common.pageObjects.domesticFlightsTab));
//     browser.waitForVisible(common.pageObjects.domesticFlightsTab, 30000);
//     console.log('domestic tab found');
//     browser.click(common.pageObjects.domesticFlightsTab);
//     browser.waitForVisible(common.pageObjects.firstBookable, 30000);
//     // client.execute(function(arg1){
//     //   jQuery(arg1).click()
//     // }, common.pageObjects.firstBookable);
//     client.click(common.pageObjects.firstBookable);
// });

this.Then(/^I click a return bookable product$/, function () {
  browser.waitForVisible(common.pageObjects.internationalFlightsTab, 30000);
  browser.click(common.pageObjects.internationalFlightsTab);
  browser.waitForVisible(common.pageObjects.returnBookable, 30000);
  client.click(common.pageObjects.returnBookable);
});

this.Then(/^I submit a booking search$/, function () {
  browser.waitForVisible(common.pageObjects.startDate, 30000);
  client.execute(function(){
    jQuery("#edit-flighttype").val('return').trigger("chosen:updated");
  });
  browser.click(common.pageObjects.startDate, 5000);
  browser.waitForVisible('.ui-datepicker', 5000);
  browser.click(common.pageObjects.datePickerStartDate, 5000);
  browser.waitForVisible('.ui-datepicker', 5000);
  browser.click(common.pageObjects.datePickerEndDate, 5000);
  browser.waitForVisible(common.pageObjects.findAndBookOnline, 30000);
  browser.click(common.pageObjects.findAndBookOnline);
});

this.Then(/^I should be on the soar search form$/, function () {
  var url = browser.url()
  expect(url).toContain('/results');
});

this.Then(/^I click the first one way bookable product$/, function () {
  browser.waitForVisible(common.pageObjects.domesticFlightsTab, 30000);
  browser.click(common.pageObjects.domesticFlightsTab)
  browser.waitForVisible(common.pageObjects.oneWayProduct, 30000);
  client.click(common.pageObjects.oneWayProduct);
});

this.Then(/^I submit a one way booking search$/, function () {
  browser.waitForVisible(common.pageObjects.startDate, 30000);
  browser.click(common.pageObjects.startDate);
  browser.click(common.pageObjects.datePickerStartDate, 5000);
  //Executes JS in the browser to check that the end date is not enabled for One-Way
  var val = client.execute(function(){
    if (jQuery('.form__end-date').is(':enabled')){
      return false;
    }  else {
        return true;
      }

    });
    var isEnabled = val.value;
    expect(isEnabled).toBe(true);
  browser.waitForVisible(common.pageObjects.findAndBookOnline, 30000);
  browser.click(common.pageObjects.findAndBookOnline);
});

}
