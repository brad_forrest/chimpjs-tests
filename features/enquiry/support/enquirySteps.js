var common = require("../../lib/pageObjects.js")

module.exports = function() {

  this.Then(/^I fill out the enquiry form$/, function () {
    client.waitForExist(common.pageObjects.firstName);
    client.setValue(common.pageObjects.enquiryDeparting, 'Brisbane');
    client.setValue(common.pageObjects.enquiryDestination, 'Sydney');
    client.setValue(common.pageObjects.comments, 'test.mavericks');
    client.setValue(common.pageObjects.firstName, 'test.mavericks');
    client.setValue(common.pageObjects.lastName, 'test.mavericks');
    client.setValue(common.pageObjects.postcode, '4000');
    client.setValue(common.pageObjects.phone, '0400000000');
    client.setValue(common.pageObjects.email, 'fcl.websolutions@gmail.com');
  });

  this.Then(/^I click submit$/, function () {
    browser.waitForVisible(common.pageObjects.enquirySubmitButton, 30000);
    client.click(common.pageObjects.enquirySubmitButton);
  });

  this.Then(/^I see "([^"]*)"$/, function (link) {
    browser.waitForExist('a=' + link);
  });

  this.Then(/^I select an exact date of today$/, function () {
    client.waitForVisible('.form-item-travelDateKnown');
    browser.click('.form-item-travelDateKnown:nth-child(2)');
    client.waitForVisible('#edit-rough-month');
    browser.click('#edit-rough-month');
    client.waitForVisible('.form-item-rough-month > div > ul > li:nth-child(7) > a');
    browser.click('.form-item-rough-month > div > ul > li:nth-child(7) > a');
    client.waitForVisible('div.submit > a');
    browser.click('div.submit > a');
  });


  this.When(/^I click on the "([^"]*)" checkbox$/, function (selector) {
    client.click(common.pageObjects[selector]);
  });

  this.Then(/^I should see "([^"]*)" is a required field$/, function (selector) {
    browser.waitForVisible(common.pageObjects[selector], 30000);
    var found = (client.getText(common.pageObjects[selector]) == '*');
    expect(found).toBe(true);
  });

  this.Then(/^I should not see "([^"]*)" is a required field$/, function (selector) {
    var found = client.isExisting(common.pageObjects[selector]);
    expect(found).not.toBe(true);
  });

  this.Given(/^I set the "([^"]*)" dropdown to display$/, function (selector) {
    var el = common.pageObjects[selector];
    client.execute(function(arg){
      document.querySelector(arg).style.display= 'block';
    }, el)
  });

  this.Then(/^the "([^"]*)" hidden field should contain the value "([^"]*)"$/, function (selector, val) {
    expect(client.getValue(common.pageObjects[selector])).toContain(val);
  });

  this.Then(/^I click the "([^"]*)" progressive disclosure button$/, function (selector) {
    client.click(common.pageObjects[selector])
  });

  this.Given(/^I fill in the "([^"]*)" with 250 characters$/, function (selector) {
    var text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit." +
          " Proin ac lectus eu ex congue suscipit. Phasellus lobortis leo posuere " +
          "metus blandit placerat. Donec in laoreet nulla. " +
          "Phasellus vel est varius, suscipit nunc ut, ultrices dolor. Suspendisse sed.";

    client.setValue(common.pageObjects[selector], text);

  });

  this.Then(/^I should see "([^"]*)" in the hidden keyword$/, function (selector) {
    expect(client.getValue(common.pageObjects[selector])).toEqual('250characters');
  });

  this.Then(/^I should see the enquiry confirmation$/, function () {
    // client.waitForExist('h1', 30000);
    // var text = client.getText('h1');
    // console.log(client.getText('h1').indexOf('Confirmation') > -1);
    // client.waitUntil(function(){
    //   client.getText('h1').indexOf('Confirmation') > -1;
    // }, 30000)
    browser.waitUntil(function(){
      // if (expect(client.getText(common.pageObjects.enquiryConfirmation)).toContain('Thanks for your travel enquiry')){
      if ((client.getText(common.pageObjects.enquiryConfirmation)).indexOf('Thanks for your travel enquiry') > -1){
        return true;
      }
    }, 30000);
    expect(client.getText(common.pageObjects.enquiryConfirmation)).toContain('Thanks for your travel enquiry');

    // var header = client.getText('h1');
    // expect(header).toContain('Confirmation');
    // var found = browser.url().value.indexOf(common.siteSettings.enquiryConfirmation) > -1;
    // expect(found).toBe(true);
  });

  this.Then(/^I fill out the enquiry form with a title$/, function () {
    client.setValue(common.pageObjects.enquiryDeparting, 'Brisbane');
    browser.uiAutocomplete('Brisbane');
    client.setValue(common.pageObjects.enquiryDestination, 'Sydney');
    browser.uiAutocomplete('Sydney');
    client.setValue(common.pageObjects.comments, 'test.mavericks');
    client.setValue(common.pageObjects.firstName, 'test');
    client.setValue(common.pageObjects.lastName, 'mavericks');
    client.setValue(common.pageObjects.postcode, '4000');
    client.setValue(common.pageObjects.phone, '0400000000');
    client.setValue(common.pageObjects.email, 'fcl.websolutions@gmail.com');
    client.selectByValue(common.pageObjects.salutation, 'Mr');
  });

  this.When(/^I fill the "([^"]*)" field with "([^"]*)"$/, function (location, value) {
    client.waitForVisible(common.pageObjects[location], 30000);
    client.setValue(common.pageObjects[location], value);
  });

  this.Given(/^I should see the value "([^"]*)" in the local storage$/, function (value) {
    var user = browser.localStorage('GET', 'user');
    expect(user.value).toContain(value);
  });

  this.Then(/^I should see the value new number in the local storage$/, function () {
    var user = browser.localStorage('GET', 'user');
    expect(user.value).toContain('0411111111');
  });

  this.Given(/^I click the first product$/, function () {
    browser.waitForVisible(common.pageObjects.procatProductView, 30000);
    browser.click(common.pageObjects.procatProductView);
  });

  this.Then(/^I should see the "([^"]*)" field matches the "([^"]*)" field$/, function (selector1, selector2) {
    var text1 = browser.getText(common.pageObjects[selector1]);
    var text2 = browser.getText(common.pageObjects[selector2]);
    expect(text1).toContain(text2);
  });

  this.Then(/^I should see the "([^"]*)" field is blank$/, function (selector) {
    var value = browser.getValue(common.pageObjects[selector]);
    expect(value).not.toBeTruthy();
  });

  this.Then(/^I add "([^"]*)" as the "([^"]*)"$/, function (value, selector) {
    browser.setValue(common.pageObjects[selector], value);
    var element = browser.element(common.pageObjects[selector]);
  });

  this.Then(/^I fill out the rest of the enquiry form$/, function () {
    client.setValue(common.pageObjects.comments, 'test.mavericks');
    client.setValue(common.pageObjects.firstName, 'test');
    client.setValue(common.pageObjects.lastName, 'mavericks');
    client.setValue(common.pageObjects.postcode, '4000');
    client.setValue(common.pageObjects.phone, '0400000000');
    client.setValue(common.pageObjects.email, 'fcl.websolutions@gmail.com');
    client.selectByValue(common.pageObjects.salutation, 'Mr');
  });

  this.Then(/^I select a date of "([^"]*)" on "([^"]*)"$/, function (date, selector) {
    browser.waitForExist(common.pageObjects[selector], 10000);
    browser.click(common.pageObjects[selector]);
    browser.click(common.pageObjects.datepickerNextMonth);
    browser.click(common.pageObjects.datepickerNextMonth);
    browser.click(common.pageObjects.datePickerStartDate);
  });




}
