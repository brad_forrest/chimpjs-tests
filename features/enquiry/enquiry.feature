#@watch
Feature: Submit an enquiry

  As a User
  I want to be able to submit an enquiry form
  So I can have my enquiry responded to

  @watch
  Scenario: I set the state for tests
    Given I am on the "home" page
    Then I setup the tests
    # And I inject the popup cookie
    # And I clear the cache

  Scenario Outline: Check that inputting incorrect data, prompts validation
    # And I clear the cache
    Given I am on the "<location>" page
    When I click submit
    And I wait for a period of time
    Then I should see "Please enter your first name" on "firstNameError"
    Then I should see "Please enter your last name" on "lastNameError"
    Then I should see "Please provide a postcode" on "postcodeError"
    Then I should see "Please enter a valid telephone number including area code" on "phoneError"

  Examples:
    | location |
    | enquiry |

  Scenario Outline: Check that group setting is being applied to enquiries where travellers are greater than 10
    # And I clear the cache
    Given I am on the "<location>" page
    And I set the "travellers" dropdown to display
    When I fill in the "adults" field with "9"
    When I fill in the "children" field with "9"
    Then I click the "passengers"
    Then the "keyword" hidden field should contain the value "groups"

  Examples:
    | location |
    | enquiry |

  Scenario Outline: Check that adding more than 250 characters to comments correctly sets the hidden field
    # And I clear the cache
    Given I am on the "<location>" page
    Then I fill out the enquiry form with a title
    And I fill in the "comments" with 250 characters
    Then I should see "twoHundredFiftyCharacters" in the hidden keyword

  Examples:
    | location |
    | enquiry |

  Scenario Outline: Check that area code is added to hidden field
    Given I am on the "<location>" page
    When I fill the "phone" field with "<phone>"
    And I select an exact date of today
    Then the "countryCode" hidden field should contain the value "+61"
    Then the "areaCode" hidden field should contain the value "<value>"

  Examples:
    | location | phone | value |
    | enquiry | 0730000000 | 7 |
    | enquiry | 0400000000 | 4 |
    | enquiry | 0260000000 | 2 |

  Scenario Outline: Check that area code is correctly derived from postcode
    Given I am on the "<location>" page
    When I fill the "phone" field with "30833341"
    When I fill the "postcode" field with "<postcode>"
    And I select an exact date of today
    Then the "countryCode" hidden field should contain the value "+61"
    Then the "areaCode" hidden field should contain the value "<value>"

  Examples:
    | location | postcode | value |
    | enquiry | 4000 | 7 |
    | enquiry | 2000 | 2 |
    | enquiry | 0999 | 8 |

  Scenario Outline: Submit an enquiry form with a budget
    # And I clear the cache
    Given I am on the "<location>" page
    Then I fill out the enquiry form with a title
    And I select an exact date of today
    Then I click the "budget" progressive disclosure button
    Then I fill in the "budgetField" field with "5000"
    Then I click submit
    Then I should see the enquiry confirmation

  Examples:
    | location |
    | enquiry |

  Scenario Outline: Submit an enquiry form
    #Then I close the popup
    Given I clear the cache
  	And I am on the "<location>" page
  	Then I fill out the enquiry form with a title
  	And I select an exact date of today
  	When I click submit
    Then I wait for a period of time
    Then I should see the enquiry confirmation

  Examples:
    | location |
    | enquiry |

  Scenario: Edit enquiry customer card and submit enquiry form
    Given I am on the "enquiry" page
    And I should see the "customerCard"
    And I should see the value "0400000000" in the local storage
    When I click the "customerCard"
    Then I should see the "salutation"
    Then I fill out the enquiry form with a title
  	And I select an exact date of today
  	When I click submit
    Then I wait for a period of time
    Then I should see the enquiry confirmation

  Scenario: Edit enquiry customer card and submit enquiry form
    Given I am on the "enquiry" page
    And I should see the value "0400000000" in the local storage
    When I click the "customerCard"
    And I wait for a period of time
    Then I fill in the "phone" field with "0411111111"
    And I fill in the "comments" field with "test.mavericks"
    And I fill in the "enquiryDestination" field with "Brisbane"
  	And I select an exact date of today
  	When I click submit
    Then I wait for a period of time
    Then I should see the enquiry confirmation
    And I should see the value new number in the local storage

  Scenario: Check product enquiry form for errors
    Given I am on the "holidays" page
    And I click the first product
    Then I should see the "productDestination" field matches the "enquiryDestination" field
    And I should see the "enquiryDepartureDate" field is blank

  @ignore
  Scenario: Submit a empty Round the world enquiry form to check validation
    Given I am on the "roundTheWorld" page
    Then I click the "addAnotherDestination"
    When I click submit
    Then I should see "Please enter your first name" on "firstNameError"
    Then I should see "Please enter your last name" on "lastNameError"
    Then I should see "Please provide a postcode" on "postcodeError"
    Then I should see "Please enter a valid telephone number including area code" on "phoneError"
    Then I should see "This field is required." on "roundTheWorldDestinationError"
    Then I should see "This field is required." on "roundTheWorldDestinationDateError"
    Then I should see "This field is required." on "roundTheWorldSecondDestinationError"

  @ignore
  Scenario: Submit a Round the world enquiry form
    Given I am on the "roundTheWorld" page
    Then I add "Brisbane" as the "roundTheWorldDestination"
    Then I select a date of "12/12/2017" on "roundTheWorldDestinationDate"
    When I click the "addAnotherDestination"
    Then I add "Tokyo" as the "roundTheWorldSecondDestination"
    Then I select a date of "12/12/2017" on "roundTheWorldSecondDestinationDate"
    Then I fill out the rest of the enquiry form
    And I select an exact date of today
    When I click submit
    Then I wait for a period of time
    Then I should see the enquiry confirmation






