var common = require("../../lib/pageObjects.js");

module.exports = function() {

this.Given(/^I check the "([^"]*)" checkbox with jquery$/, function (checkbox) {
  client.execute(function(arg1){
    // return jQuery('[value="' + arg1 + '"]').attr('checked', true);
    return jQuery('[value="' + arg1 + '"]').click();
  }, checkbox);
});

this.Given(/^I check the no price checkbox$/, function () {
  client.execute(function(){
    // return jQuery('[value="' + arg1 + '"]').attr('checked', true);
    return jQuery('#fcl-facets-facet-apachesolracquia-search-server-1-block-fs-price *[value="[* TO 0]"]').click();
  });
});

this.Then(/^I should see that the "([^"]*)" checkbox is checked$/, function (checkbox) {
  var isChecked = client.execute(function(arg1){
    return jQuery('[value="' + arg1 + '"]:checked').length > 0;
  }, checkbox);
  expect(isChecked.value).toBe(true);
});

this.Then(/^I should see that the no price checkbox is checked$/, function () {
  var isChecked = client.execute(function(){
    return jQuery('#fcl-facets-facet-apachesolracquia-search-server-1-block-fs-price *[value="[* TO 0]"]:checked').length > 0;
  });
  expect(isChecked.value).toBe(true);
});

this.When(/^I uncheck the "([^"]*)" checkbox with jquery$/, function (checkbox) {
  client.execute(function(arg1){
    return jQuery('[value="' + arg1 + '"]').attr('checked', false);
  }, checkbox);
});

this.Then(/^I should see that the "([^"]*)" checkbox is unchecked$/, function (checkbox) {
  var isNotChecked = client.execute(function(arg1){
    return jQuery('[value="' + arg1 + '"]:checked').length > 0;
  }, checkbox);
  expect(isNotChecked.value).toBe(false);
});



}
