var common = require("../../lib/pageObjects.js")

module.exports = function() {

  this.Then(/^I select the first "([^"]*)"$/, function (selector) {
    browser.waitForVisible(common.pageObjects[selector], 30000);
    // var el = common.pageObjects[selector];
    // browser.execute(function(arg1){
    //   jQuery(arg1).click();
    // }, el);
    browser.click(common.pageObjects[selector]);
  });

  this.Then(/^I fill out the lombard enquiry form$/, function () {
    client.waitForExist(common.pageObjects.productEnquiryFirstName);
    client.setValue(common.pageObjects.enquiryDeparting, 'Brisbane');
    browser.uiAutocomplete('Brisbane');
    client.setValue(common.pageObjects.enquiryDestination, 'Sydney');
    browser.uiAutocomplete('Sydney');
    client.setValue(common.pageObjects.productEnquiryComments, 'test.mavericks');
    client.setValue(common.pageObjects.productEnquiryFirstName, 'test');
    client.setValue(common.pageObjects.productEnquiryLastName, 'mavericks');
    client.setValue(common.pageObjects.productEnquiryPostcode, '4000');
    client.setValue(common.pageObjects.productEnquiryPhone, '0400000000');
    client.setValue(common.pageObjects.productEnquiryEmail, 'fcl.websolutions@gmail.com');
    client.selectByValue(common.pageObjects.salutation, 'Mr');
    client.click(common.pageObjects.productLombardCheckbox);

  });

  this.Then(/^I should see the "([^"]*)"$/, function (selector) {
    browser.waitForVisible(common.pageObjects[selector], 10000);
    var found = client.isExisting(common.pageObjects[selector]);
    expect(found).toBe(true);
  });

  this.Then(/^I should see the monthly pricing is correct$/, function () {
    var dataPrice= browser.getAttribute(common.pageObjects.procatProducts, 'data-pricedigits');
    // console.log(dataPrice);
    var monthlyInterestFreePrice = browser.getText(common.pageObjects.monthlyInterestFreePriceHeader)[0];
    var monthlyInterestFreePriceStr = JSON.stringify(monthlyInterestFreePrice);
    function cleanPrice(price){
      price = Number(price.replace(/[^0-9\.]+/g,""));
      price = parseFloat(price);
      price = Math.round(price);
      return price;
    }
    var monthlyInterestFreePriceInt =  cleanPrice(monthlyInterestFreePriceStr);
    var interestFreeLength = '';
    if (browser.isExisting(common.pageObjects.lombardInterestFree9Month)){
      interestFreeLength = 9;
    }
    else if(browser.isExisting(common.pageObjects.lombardInterestFree12Month)){
      interestFreeLength = 12;
    }
    else if(browser.isExisting(common.pageObjects.lombardInterestFree6Month)){
      interestFreeLength = 6;
    }
    var dataPriceInt = parseInt(dataPrice);
    var totalPrice = dataPriceInt;
    var monthlyPriceCalc = totalPrice / interestFreeLength;
    var monthlyPriceCalcFloat = parseFloat(monthlyPriceCalc);
    var monthlyPriceCalcRound = Math.ceil(monthlyPriceCalcFloat);
    expect(monthlyPriceCalcRound).toEqual(monthlyInterestFreePriceInt);
  });

  this.Then(/^I should see the weekly pricing is correct$/, function () {
    var dataPrice= browser.getAttribute(common.pageObjects.procatProducts, 'data-pricedigits');
    var weeklyInterestFreePrice = browser.getText(common.pageObjects.weeklyInterestFreePriceHeader)[0];
    var weeklyInterestFreePriceStr = JSON.stringify(weeklyInterestFreePrice);
    function cleanPrice(price){
      price = Number(price.replace(/[^0-9\.]+/g,""));
      price = parseFloat(price);
      price = Math.round(price);
      return price;
    }
    var weeklyInterestFreePriceInt =  cleanPrice(weeklyInterestFreePrice);
    var interestFreeLength = '';
    if (browser.isExisting(common.pageObjects.lombardInterestFree9Month)){
      interestFreeLength = 9;
    }
    else if(browser.isExisting(common.pageObjects.lombardInterestFree12Month)){
      interestFreeLength = 12;
    }
    else if(browser.isExisting(common.pageObjects.lombardInterestFree6Month)){
      interestFreeLength = 6;
    }
    var dataPriceInt = parseInt(dataPrice);
    var totalPrice = dataPriceInt;
    var week = 52 / 12;
    var interestFreeLengthWeekly = week * interestFreeLength;
    var weeklyPriceCalc = totalPrice / interestFreeLengthWeekly;
    var weeklyPriceCalcFloat = parseFloat(weeklyPriceCalc);
    var weeklyPriceCalcRound = Math.round(weeklyPriceCalcFloat);
    expect(weeklyPriceCalcRound).toEqual(weeklyInterestFreePriceInt);
  });

};
