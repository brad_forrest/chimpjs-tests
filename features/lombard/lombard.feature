#@ignore
#@watch
Feature: Automated testing of lombard interest free products
  In order to pay for a holiday with an interest free loan
  As a user
  I need to be able to book an interest free product

  Scenario: Enquire on an interest free product
    Given I am on the "holidaysSearch" page
    And I check the "[1501 TO 2000]" checkbox with jquery
    And I wait for AJAX to finish
    Then I wait for a period of time
    Then I select the first "procatProductView"
    Then I should see the "lombardLogo"
    Then I fill out the lombard enquiry form
    And I select an exact date of today
  	When I click submit
    Then I should see the enquiry confirmation

  Scenario: Checking that Monthly pricing is correct
    Given I am on the "holidaysSearch" page
    And I check the "[1501 TO 2000]" checkbox with jquery
    And I wait for AJAX to finish
    Then I wait for a period of time
    Then I select the first "procatProductView"
    Then I should see the monthly pricing is correct

  Scenario: Checking that weekly pricing is correct
    Given I am on the "holidaysSearch" page
    And I check the "[1501 TO 2000]" checkbox with jquery
    And I wait for AJAX to finish
    Then I wait for a period of time
    Then I select the first "procatProductView"
    Then I should see the weekly pricing is correct
