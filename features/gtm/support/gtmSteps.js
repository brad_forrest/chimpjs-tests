module.exports = function() {
  this.Then(/^I assert that the "([^"]*)" value matches the fcl procat product from the "([^"]*)" element$/, function (gtmFieldName, dataFieldName) {
    var gtm = gtmFieldName;
    var data = dataFieldName;
    var test = browser.execute(function(arg1, arg2){
       for (var i in dataLayer){
            var obj = dataLayer[i];
            if (obj instanceof Object && typeof obj[arg1] != 'undefined'){
                if (obj[arg1] == jQuery('.fcl-procat-product').data(arg2)) {
                    return true;
                    
                    }
            }
        }
        return false;
    }, gtm, data);
    expect(test.value).toBe(true);    
  });
}
